﻿
var fs = require("fs");
var geojsonVt = require('geojson-vt')
exports.ptile = null;
exports.dtile = null;
exports.wtile = null;
exports.djson = null;
exports.wjson = null;
exports.setSpatialIndex = function () {
    var me = this;
    var tfilePath = __dirname + '/../data/tinh.geojson';
    //province index
    fs.exists(tfilePath, function (exists) {
        if (exists) {
            var orig = JSON.parse(fs.readFileSync(tfilePath));
            me.ptile = geojsonVt(orig);
        }
    })

    //district index
    var dfilePath = __dirname + '/../data/huyen.geojson';
    fs.exists(dfilePath, function (exists) {
        if (exists) {
            var orig = JSON.parse(fs.readFileSync(dfilePath));
            me.djson = orig;
            me.dtile = geojsonVt(orig);
        }
    })

    //ward index
    var wfilePath = __dirname + '/../data/xa.geojson';
    fs.exists(wfilePath, function (exists) {
        if (exists) {
            var orig = JSON.parse(fs.readFileSync(wfilePath));
            // me.wtile = geojsonVt(orig);
            me.wtile = geojsonVt(orig, {
                maxZoom: 18,  // max zoom to preserve detail on; can't be higher than 24
                tolerance: 3, // simplification tolerance (higher means simpler)
                extent: 4096, // tile extent (both width and height)
                buffer: 64,   // tile buffer on each side
                debug: 0,     // logging level (0 to disable, 1 or 2)
                lineMetrics: false, // whether to enable line metrics tracking for LineString/MultiLineString features
                promoteId: null,    // name of a feature property to promote to feature.id. Cannot be used with `generateId`
                generateId: true,  // whether to generate feature ids. Cannot be used with `promoteId`
                indexMaxZoom: 5,       // max zoom in the initial tile index
                indexMaxPoints: 100000 // max number of points per tile in the index
            });
            me.wjson = orig;
        }
    })
}