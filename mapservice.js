﻿
var http = require('http');
var express = require('express');
var app = express();
var server = http.createServer(app);
var bodyParser = require('body-parser');
var compression = require('compression')
var cors = require('cors');
var commons = require("./commons/commons");
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json());
app.use(compression());
app.use(cors());

//limit 
app.use(bodyParser.json({ limit: '50mb' }));
app.set("jsonp callback", true);

// Routing
var mapRouter = require('./routes/map');
app.use('/', mapRouter);

server.listen("1004", function () {
    commons.setSpatialIndex();
    console.log('%s listening at port: 1004 %s ', " Webservice", "");
});

app.on('uncaughtException', function (req, res, route, err) {
    console.error('uncaughtException: %s', err.stack);
})

app.on('error', function (req, res, route, err) {
    console.error('uncaughtException: %s', err.stack);
})

