var express = require('express');
var routes = express.Router();
var GeoJSONProvider = require('../collection_provider/GeoJSONProvider');
routes.get('/rest/test', function (req, res, next) {
    res.send("ok");
});
routes.get('/rest/map/:mapid/:layerid/tile/:zoom/:col/:row', function (req, res, next) {
    GeoJSONProvider.querytile(req, res, next);
});

routes.get('/rest/map/:mapid/:layerid/features', function (req, res, next) {
    GeoJSONProvider.queryFeatureByBBOX(req, res, next);
});

module.exports = routes;