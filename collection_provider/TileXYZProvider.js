var fs = require('fs');
var path = require('path');
function responeTile(fileName, res, next) {
    if (fs.existsSync(fileName)) {
        fs.readFile(fileName, function (err, data) {
            if (err) throw err;
            res.end(data);
        });
    } else {
        // var noimagepath = 'D:\\Dev\\GSV15\\gServices\\App_Data\\no_image.png';
        var noimagepath = path.join(__dirname, '../App_Data/no_image.png');
        fs.readFile(noimagepath, function (err, data) {
            if (err) throw err;
            res.end(data);
        });
    }
}

exports.GetMapTile = function (req, res, next) {
    var mapid = req.params.mapid;
    var configFile = path.join(__dirname, '../config/' + mapid + '.json');
    if (fs.existsSync(configFile)) {
        fs.readFile(configFile, { encoding: 'utf-8' }, function (err, mapcontent) {
            if (err) {
                console.log(err);
            } else {
                var mapconfig = JSON.parse(mapcontent);
                var folder = mapconfig.ServiceUrl;
                var level = req.params.level;
                var col = req.params.col;
                var row = req.params.row.split('.')[0];
                var format = req.params.row.split('.')[1];
                var zoom = parseInt(level);

                var folderName = folder + '\\' + level + '\\' + col;
                var fileName = '';
                try {
                    fileName = folderName + '\\' + row + '.' + format;
                    responeTile(fileName, res, next);
                }
                catch (ex) {
                    res.send({ code: 100, message: ex });
                }
            }
        });
    } else {
        res.send({ code: 100, message: 'Config file error or not found!' });
    }

}

function zeroPad(e, t, n) {
    for (e = e.toString(n || 10); e.length < t;) {
        e = "0" + e;
    }
    return e;
}