var request = require("request");
var geocoderProvider = 'google';
var httpAdapter = 'https';
var extra = {
    apiKey: 'AIzaSyDyMkLHjvq2irh8dbwx44PHqvkgqecgXoA', // for Mapquest, OpenCage, Google Premier
    language: 'vi',
    formatter: null         // 'gpx', 'string', ...
};
var geocoder = require('node-geocoder').getGeocoder(geocoderProvider, httpAdapter, extra);

exports.getDiaChi = function (req, res, next) {
    geocoder.reverse(req.query.lat, req.query.lon, function (err, res2) {
        if (err || res2 == undefined) {
            res.send(200, { "result": [], "message": err });
            return next();
        }
        else {
            res.send(200, { "result": res2, "message": '' });
            return next();
        }
    });
}

exports.getLongLat = function (req, res, next) {
    geocoder.geocode(req.params.address, function (err, res2) {
        if (err || res2 == undefined) {
            res.send(200, { "result": [], "message": err });
            return next();
        }
        else {
            res.send(200, { "result": res2, "message": '' });
            return next();
        }
    });
}

exports.getDirection = function (req, rest, next) {
    var origin = req.query.origin;
    var destination = req.query.destination;
    var url = "https://maps.googleapis.com/maps/api/directions/json?origin=" + encodeURIComponent(origin) + "&destination=" + encodeURIComponent(destination) + "&key=" + extra.apiKey + "&units=metric&language=vi";
    // make request to google server
    request({ "url": url, "timeout": 120000 }, function (err, res, body) {
        if (res == null || res.statusCode != 200) {
            rest.send(body);
        }
        else {
            var objDirection = JSON.parse(body);
            if (objDirection.status != "OK") {
                res.send(body);
               // rest.send({ "distance": distance, "route": route });
            }
            else {
                distance = objDirection.routes[0].legs[0].distance.value;
                route = decodepolyline(objDirection.routes[0].overview_polyline.points);
                var instructions = getInstructions(objDirection.routes[0]);
                rest.send({ "distance": distance, "route": route, "instructions": instructions });
            }
        }
    });

}


function getInstructions(route) {
    var htmlInstructions = '';
    if (route && route.legs.length > 0) {
        for (var i = 0; i < route.legs[0].steps.length; i++) {
            htmlInstructions += route.legs[0].steps[i].html_instructions;
        }
    }
    return htmlInstructions;
}

function decodepolyline(str, precision) {
    var index = 0,
        lat = 0,
        lng = 0,
        coordinates = [],
        shift = 0,
        result = 0,
        byte = null,
        latitude_change,
        longitude_change,
        factor = Math.pow(10, precision || 5);

    // Coordinates have variable length when encoded, so just keep
    // track of whether we've hit the end of the string. In each
    // loop iteration, a single coordinate is decoded.
    while (index < str.length) {

        // Reset shift, result, and byte
        byte = null;
        shift = 0;
        result = 0;

        do {
            byte = str.charCodeAt(index++) - 63;
            result |= (byte & 0x1f) << shift;
            shift += 5;
        } while (byte >= 0x20);

        latitude_change = ((result & 1) ? ~(result >> 1) : (result >> 1));

        shift = result = 0;

        do {
            byte = str.charCodeAt(index++) - 63;
            result |= (byte & 0x1f) << shift;
            shift += 5;
        } while (byte >= 0x20);

        longitude_change = ((result & 1) ? ~(result >> 1) : (result >> 1));

        lat += latitude_change;
        lng += longitude_change;

        coordinates.push([lat / factor, lng / factor]);
    }

    return coordinates;
};