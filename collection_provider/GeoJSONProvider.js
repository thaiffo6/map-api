var fs = require("fs");
var path = require("path");
var mkdirp = require("mkdirp");
var VectorTile = require('@mapbox/vector-tile').VectorTile
var commons = require("../commons/commons");
var Pbf = require("pbf");
var vtpbf = require('vt-pbf')
var globalMercator = require('global-mercator')

//Public function
exports.querytile = async function (req, res, next) {
  var layerid = req.params.layerid;
  var zoom = req.params.zoom;
  var tx = req.params.col;
  var ty = req.params.row;
  var tileindex = null;
  if (layerid.indexOf("tinh") != -1) {
    tileindex = commons.ptile;
  }
  else if (layerid.indexOf("huyen") != -1) {
    tileindex = commons.dtile;
  }
  else if (layerid.indexOf("xa") != -1) {
    tileindex = commons.wtile;
  }
  var tile = tileindex.getTile(parseInt(zoom), parseInt(tx), parseInt(ty));
  if (!tile || tile == null) return res.status(204).send();  // 204 empty status for mapbox
  else {
    var buff;
    if (layerid.indexOf("tinh") != -1)
      buff = vtpbf.fromGeojsonVt({ "tinh": tile })
    else if (layerid.indexOf("huyen") != -1)
      buff = vtpbf.fromGeojsonVt({ "huyen": tile })
    else if (layerid.indexOf("xa") != -1)
      buff = vtpbf.fromGeojsonVt({ "xa": tile })
    if (buff) {
      buff = new Buffer.from(buff.buffer);
      res.setHeader("Content-Type", "application/x-protobuf");
      res.send(buff);
    }
  }
}

exports.queryFeatureByBBOX = async function (req, res, next) {
  var layerid = req.params.layerid;
  var id = req.query.id;
  var features = [];
  if (layerid.indexOf("huyen") != -1) {
    json = commons.djson;
    features = json.features.filter(function (fea) {
      return fea.properties.city_id == id
    });
  }
  else if (layerid.indexOf("xa") != -1) {
    json = commons.wjson;
    features = json.features.filter(function (fea) {
      return fea.properties.district_id == id
    });
  }
  res.send(features);
}

